# Solution for Frozen lake game
# see: https://medium.com/emergent-future/simple-reinforcement-learning-with-tensorflow-part-0-q-learning-with-tables-and-neural-networks-d195264329d0
# see: https://www.analyticsindiamag.com/openai-gym-frozen-lake-beginners-guide-reinforcement-learning/

import gym
import numpy as np
import random
import tensorflow as tf
import matplotlib.pyplot as plt
#%matplotlib inline


arrowMapping={-1:"", 0:"<", 1:"v", 2:">", 3:"^"}

def renderCell(cell):
    if cell.sum() == 0:
        return " "
    else:
        return arrowMapping[np.argmax(cell)]

def renderArrayRow(rowNr,rowArray):
    rowToArrowMapped=[renderCell(cell) for cell in rowArray]
    print ("".join( rowToArrowMapped ))


# To override the default "SLIPPERY" condition
#env = gym.make('FrozenLake-v0')
from gym.envs.registration import register
env = register(
    id='FrozenLakeOverriden-v0',
    entry_point='gym.envs.toy_text:FrozenLakeEnv',
     kwargs={'map_name' : '4x4', 'is_slippery': True},
     #max_episode_steps=100,
     #reward_threshold=0.78, # optimum = .8196
 )
env = gym.make('FrozenLakeOverriden-v0')

env.render()




tf.reset_default_graph()


#These lines establish the feed-forward part of the network used to choose actions
inputs1 = tf.placeholder(shape=[1,16],dtype=tf.float32)
W = tf.Variable(tf.random_uniform([16,4],0,0.01))
Qout = tf.matmul(inputs1,W)
predict = tf.argmax(Qout,1)

# Define the Next State and Error Corretion Techniques
# Below we obtain the loss by taking the sum of squares difference between the target and prediction Q values.
# Gradient optimizer to reduce the loss with a learning rate of 0.1
nextQ = tf.placeholder(shape=[1,4],dtype=tf.float32)
loss = tf.reduce_sum(tf.square(nextQ - Qout))
trainer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
updateModel = trainer.minimize(loss)


init = tf.initialize_all_variables()

# Set learning parameters
y = .99
e = 0.1
num_episodes = 2000
#create lists to contain total rewards and steps per episode
jList = []
rList = []
with tf.Session() as sess:
    sess.run(init)
    for i in range(num_episodes):
        #Reset environment and get first new observation
        s = env.reset()
        rAll = 0
        d = False
        j = 0
        #The Q-Network
        while j < 99:
            j+=1
            #Choose an action by greedily (with e chance of random action) from the Q-network
            a,allQ = sess.run([predict,Qout],feed_dict={inputs1:np.identity(16)[s:s+1]})
            if np.random.rand(1) < e:
                a[0] = env.action_space.sample()
            #Get new state and reward from environment
            s1,r,d,_ = env.step(a[0])
            #Obtain the Q' values by feeding the new state through our network
            Q1 = sess.run(Qout,feed_dict={inputs1:np.identity(16)[s1:s1+1]})
            #Obtain maxQ' and set our target value for chosen action.
            maxQ1 = np.max(Q1)
            targetQ = allQ
            targetQ[0,a[0]] = r + y*maxQ1
            #Train our network using target and predicted Q values
            _,W1 = sess.run([updateModel,W],feed_dict={inputs1:np.identity(16)[s:s+1],nextQ:targetQ})
            rAll += r
            s = s1
            if d == True:
                #Reduce chance of random action as we train the model.
                e = 1./((i/50) + 10)
                break
        jList.append(j)
        rList.append(rAll)
print("Percent of succesful episodes: " + str(100*sum(rList)/num_episodes) + "%")

###########################################################################
# Percent of succesful episodes: 0.4675%
###########################################################################

plt.plot(rList)
plt.plot(jList)
plt.show()
