import random

import gym
import time
import numpy as np


################################################################
# Original env:
################################################################
#env = gym.make('FrozenLake-v0')
################################################################


################################################################
# Overriden env - to set SLIPPERY = FALSE for 1st try
# see https://github.com/openai/gym/tree/master/gym/envs
################################################################
from gym.envs.registration import register
env = register(
    id='FrozenLakeNotSlippery-v0',
    entry_point='gym.envs.toy_text:FrozenLakeEnv',
     kwargs={'map_name' : '4x4', 'is_slippery': False},
#     max_episode_steps=100,
#     reward_threshold=0.78, # optimum = .8196
 )
env = gym.make('FrozenLakeNotSlippery-v0')
################################################################


#Initialize Q table - all zeros
Q = np.zeros([env.observation_space.n,env.action_space.n])

# Set learning parameters
lr = .8
discount = .95
num_episodes = 1
#num_episodes = 1

#create lists to contain total rewards and steps per episode
#jList = []
rList = []

#env.reset()

env.render()
for episodeNr in range(num_episodes):

    # Reset environment and get first new observation
    state = env.reset()
    #env.render()
    rewardAll = 0
    episodeDone = False
    j = 0

    # The Q-Table learning algorithm
    while j < 99:
        j += 1

        # Actions:
        # 0-left, 1-down, 2-right, 3-up,
        # Choose an action by greedily (with noise) picking from Q table
        action = np.argmax(Q[state, :] + np.random.randn(1, env.action_space.n) * (1. / (episodeNr + 1)))

        # Get new state and reward from environment
        newState, reward, episodeDone, info = env.step(action)

        # The state is number 0..16 - it's index of field where we are in the grid

        # env.render()
        # print("reward:{}".format(reward))
        # print("state:{}".format(newState))
        # print("info:{}".format(info))

        # Bellman equation used here
        # Update Q-Table with new knowledge
        Q[state, action] = Q[state, action] + lr * (reward + discount * np.max(Q[newState, :]) - Q[newState, action])
        rewardAll += reward
        state = newState

        # if done - break the loop -> reset the env and start learning with new episode
        if episodeDone == True:
            break

    #print("Episode DONE after {} steps".format(j))
    # jList.append(j)
    rList.append(rewardAll)

print("Score over time: {}".format(str(sum(rList) / num_episodes)))

print("Final Q-Table Values")
print("\t\t0-left\t\t1-down\t\t\t2-right\t\t\t3-up")

np.set_printoptions(suppress=True,
                    #precision=2,
                    formatter={'float_kind':'{:f}'.format}
                    )


#print(Q)
print("row 1")
print (Q[0:4])
print("row 2")
print (Q[4:8])
print("row 3")
print (Q[8:12])
print("row 4")
print (Q[12:16])



# just to see the random actions:
# for i in range(1000):
#      print("Step {}".format(i))
#
#      # take a random action
#      action = env.action_space.sample()
#      print(" - Take action: {}".format(action))
#
#      env.step(action)
#      env.render()
#      print("-----------------")
#      time.sleep(1)