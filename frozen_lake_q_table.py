################################################################################################
# Solution for Frozen lake game
# (see: https://medium.com/emergent-future/simple-reinforcement-learning-with-tensorflow-part-0-q-learning-with-tables-and-neural-networks-d195264329d0)
# with exposed params to experiment if the lake is just not slippery
################################################################################################
# Frozen lake in Gym - solving with Q-learning
################################################################################################
# There is a wind which occasionally blows the agent onto a space they didn’t choose.
# S - Start
# H - Hole, fall into
# F - Frozen surface
# G - Goal , reward: 1
# The returned state is number 0..16 - it's index of field where we are in the grid
# The grid is fixed during the experiment
# The only problem (which you can disable) is a wind (or ice being slippery) - so you are
# randomly moved to some different location then you planned during the move.
################################################################################################
# The reward at every step is 0,
# except for entering the goal, which provides a reward of 1.
# Thus, we will need an algorithm that learns long-term expected rewards.
################################################################################################

################################################################################################
# Helpful theory:
#################################################################################################
#
# Bellman equation:
#
# The expected long-term reward for a given action
# is equal
# to the immediate reward from the current action
# combined with
# the expected reward from the best future action taken at the following state
#
# Eq 1. Q(s,a) = r + γ(max(Q(s’,a’))
#
# Q val for state (s), action (a)
# equals
# reward (r) plus the maximum discounted (γ) future reward expected
# according to our own table for
# the next state (s’) we would end up in.
#
# Discount (γ) variable allows us to decide how important the possible future rewards are
# compared to the present reward.
################################################################################################


import gym
import numpy as np


import matplotlib.pyplot as plt

arrowMapping = {-1: "", 0: "<", 1: "v", 2: ">", 3: "^"}


def renderCell(cell):
    if cell.sum() == 0:
        return " "
    else:
        return arrowMapping[np.argmax(cell)]


def renderArrayRow(rowNr, rowArray):
    rowToArrowMapped = [renderCell(cell) for cell in rowArray]
    print("".join(rowToArrowMapped))


# To override the default "SLIPPERY" condition
# env = gym.make('FrozenLake-v0')
from gym.envs.registration import register

env = register(
    id='FrozenLakeOverriden-v0',
    entry_point='gym.envs.toy_text:FrozenLakeEnv',
    kwargs={'map_name': '4x4', 'is_slippery': True},
    # max_episode_steps=100,
    # reward_threshold=0.78, # optimum = .8196
)


env = gym.make('FrozenLakeOverriden-v0')
env.render()

# Initialize table with all zeros
Q = np.zeros([env.observation_space.n, env.action_space.n])

# Set learning parameters
lr = .8
y = .9
num_episodes = 1600
num_iterations = 200

# create lists to contain total rewards and steps per episode
iterationsInEpisode = []
rewardsPerEpisode = []


for episode in range(num_episodes):
    state = env.reset()
    rewardsInEpisode = 0

    for iteration in range(num_iterations):

        # action - greedily (with noise) picking from Q table
        possibleActionsWithNoise = Q[state, :] + np.random.randn(1, env.action_space.n) * (1. / (episode + 1))
        action = np.argmax(possibleActionsWithNoise)

        newState, reward, done, _ = env.step(action)

        # Update Q-Table
        Q[state, action] = Q[state, action] + lr * (reward + y * np.max(Q[newState, :]) - Q[state, action])
        state = newState
        rewardsInEpisode += reward
        if done == True:
            break
    iterationsInEpisode.append(iteration)
    rewardsPerEpisode.append(rewardsInEpisode)

print("Score over time: " + str(sum(rewardsPerEpisode) / num_episodes))




###########################################################################
# Results visualisation
###########################################################################

# Printing progress - how much iterations achieved
plt.figure(1)
plt.subplot(211)
plt.plot(iterationsInEpisode , color='b')
plt.axis([0, num_episodes, 0, 200])

plt.subplot(212)
plt.plot(rewardsPerEpisode, 'ro', color='g')
plt.axis([0, num_episodes, -0.1, 1.1])
plt.show()


# plt.subplot(212)
# #val = rewardsPerEpisode / (iterationsInEpisode/200.0)
# #val = [i.count(0)/(iterationsInEpisode(i.index())/200.0) for i in enumerate(rewardsPerEpisode)]
# val = [(r ) * (i/200.0) for r, i in zip(rewardsPerEpisode, iterationsInEpisode)]
# plt.plot(val, color='g')
# plt.axis([0, num_episodes, 0, 1])
# plt.show()


# print ("Final Q-Table Values")
# print("\t\t0-left\t\t1-down\t\t\t2-right\t\t\t3-up")
print(Q)
print("")
# # numpy.ndarray
renderArrayRow(1, Q[0:4])
renderArrayRow(2, Q[4:8])
renderArrayRow(3, Q[8:12])
renderArrayRow(4, Q[12:16])

###########################################################################
# Not slippery
###########################################################################

##### 1 Solution
# SFFF
# FHFH
# FFFH
# HFFG
# Score over time: 0.9085
# Final Q-Table Values
# v
# v
# >>v
#   >


##### 2 Solution
# SFFF
# FHFH
# FFFH
# HFFG
# Score over time: 0.94
# Final Q-Table Values
# >>v
#   v
#   v
#   >

###########################################################################
# Slippery
###########################################################################

##### 1 Solution
# SFFF
# FHFH
# FFFH
# HFFG
# Score over time: 0.5245
# <^v^
# < >
# ^v<
#  >>

##### 2 Solution
# SFFF
# FHFH
# FFFH
# HFFG
# Score over time: 0.5165
# <^>^
# < >
# ^vv
#  >>
