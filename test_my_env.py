# Example to test the gym
import gym
import universe  # register the universe environments
from universe import wrappers

from gym_simple.envs.simple_env import SimpleEnv

env = gym.make('Simple-v0')
#env = wrappers.experimental.SafeActionSpace(env)
#env.configure(remotes=1)

observation_n = env.reset()

while True:
    print("Taking action...")
    action_n = [env.action_space.sample() for ob in observation_n]
    print("Action : {}".format(action_n))
    observation_n, reward_n, done_n, info = env.step(action_n)
    print("Result :{} reward:{}".format(observation_n,reward_n))
    env.render()
    exit()