################################################################################################
# Solution for TODO
################################################################################################
# ....TODO in Gym -
################################################################################################
# There is a ....TODO
# The returned state is ....TODO
################################################################################################
# The reward at every step is ....TODO
################################################################################################

################################################################################################
# Helpful theory:
#################################################################################################
#
################################################################################################

# Registered anv:
# gym-soccer-0.0.1
#

import gym
#from gym.envs.toy_text import FrozenLakeEnv
from gym.envs.toy_text import FrozenLakeEnv

from gym_soccer.envs.soccer_env import SoccerEnv

env = gym.make('Soccer-v0')
env.render()


#env = wrappers.experimental.SafeActionSpace(env)
#env.configure(remotes=1)

observation_n = env.reset()

while True:
    action_n = [env.action_space.sample() for ob in observation_n]
    print("Doing step: " + action_n)
    observation_n, reward_n, done_n, info = env.step(action_n)
    print("Result reward: " + reward_n)
    print("Result observation: " + observation_n)
    env.render()
    exit()
